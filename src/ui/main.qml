// SPDX-FileCopyrightText: 2022 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import Qt.labs.platform 1.1
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.syntaxhighlighting 1.0

import org.kde.hans 1.0

Kirigami.ApplicationWindow {
    id: root

    property bool lucky

    title: i18n("Untitled")

    width: Kirigami.Units.gridUnit * 25
    height: Kirigami.Units.gridUnit * 36
    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    FileDialog {
        id: fileDialog
        title: i18n("Open File")
        nameFilters: ["Markdown files (*.md)"]
        onAccepted: {
            Controller.open(fileDialog.file)
        }
    }

    FileDialog {
        id: saveFileDialog
        title: i18n("Save File As")
        fileMode: FileDialog.SaveFile
        defaultSuffix: "md"
        onAccepted: {
            Controller.saveAs(saveFileDialog.file, textarea.text)
        }
    }

    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    Connections {
        id: saveWindowGeometryConnections
        enabled: false // Disable on startup to avoid writing wrong values if the window is hidden
        target: root

        function onClosing() { App.saveWindowGeometry(root); }
        function onWidthChanged() { saveWindowGeometryTimer.restart(); }
        function onHeightChanged() { saveWindowGeometryTimer.restart(); }
        function onXChanged() { saveWindowGeometryTimer.restart(); }
        function onYChanged() { saveWindowGeometryTimer.restart(); }
    }

    Loader {
        active: !Kirigami.Settings.isMobile
        sourceComponent: GlobalMenu {}
    }

    Shortcut {
        sequences: [StandardKey.ZoomIn]
        onActivated: textarea.font.pointSize++
    }

    Shortcut {
        sequences: [StandardKey.ZoomOut]
        onActivated: textarea.font.pointSize--
    }

    pageStack.initialPage: Kirigami.Page {
        id: page

        padding: 0
        titleDelegate: PageHeader {}

        DropArea {
            id: droparea
            anchors.fill: parent
            onDropped: {
                if (drop.urls[0].endsWith(".md")) {
                    Controller.open(drop.urls[0])
                }
            }
        }

        QQC2.ScrollView {
            anchors.fill: parent

            QQC2.TextArea {
                id: textarea

                property int characters: 0
                property int words: 0
                property int minutes: 0

                persistentSelection: true
                placeholderText: root.lucky ? "Schieß dem Fenster…" : i18n("Start writing…")
                wrapMode: Text.Wrap
                padding: Kirigami.Units.gridUnit * 3
                bottomPadding: 0

                onTextChanged: {
                    textarea.characters = length
                    textarea.words = text.split(' ').filter(function(n) { return n != '' }).length
                    textarea.minutes = Math.ceil(words / 200)
                }

                background: Rectangle {
                    Kirigami.Theme.colorSet: Kirigami.Theme.View
                    Kirigami.Theme.inherit: false
                    color: Kirigami.Theme.backgroundColor
                }

                Connections {
                    target: Controller

                    function onFileChanged(text, title) {
                        textarea.text = text
                        root.title = title
                    }
                }

                SyntaxHighlighter {
                    textEdit: textarea
                    definition: "Markdown"
                }

                Component.onCompleted: forceActiveFocus()
            }
        }

        footer: QQC2.ToolBar {
            implicitHeight: Kirigami.Units.largeSpacing * 3
            position: QQC2.ToolBar.Footer

            RowLayout {
                id: content
                anchors.fill: parent

                Item {
                    Layout.fillWidth: true
                }

                QQC2.Label {
                    text: i18np("1 character", "%1 characters", textarea.characters)
                }

                QQC2.Label {
                    text: "•"
                }

                QQC2.Label {
                    text: i18np("1 word", "%1 words", textarea.words)
                }

                QQC2.Label {
                    text: "•"
                }

                QQC2.Label {
                    text: i18np("1 min read", "%1 min read", textarea.minutes)
                }
            }
        }
    }

    Component.onCompleted: {
        if (!Kirigami.Settings.isMobile) {
            saveWindowGeometryConnections.enabled = true
        }

        let min = Math.ceil(1)
        let max = Math.floor(1000)
        let randomNumber = Math.floor(Math.random() * (max - min) + min)

        if (randomNumber == 1) {
            root.lucky = true
        }
    }
}
